# dev1-read.hcl
# List audit backends
path "sys/audit" {
  capabilities = ["read", "list"]
}

# List audit backends. Operators are not allowed to remove them.
path "sys/audit/*" {
  capabilities = ["read", "list", "sudo"]
}

# List Authentication Backends
path "sys/auth" {
  capabilities = ["read", "list", "sudo"]
}

# Read and List operations on Authentication Backends
path "sys/auth/*" {
  capabilities = ["read", "list", "sudo"]
}

# Read CORS configuration
path "sys/config/cors" {
  capabilities = ["read", "list", "sudo"]
}

# Read License
path "sys/license" {
  capabilities = ["read", "list"]
}

# Get Storage Key Status
path "sys/key-status" {
  capabilities = ["read"]
}

# Get Cluster Leader
path "sys/leader" {
  capabilities = ["read"]
}

# Read policies
path "sys/policies*" {
  capabilities = ["read", "list"]
}

# List Mounts
path "sys/mounts*" {
  capabilities = ["read", "list"]
}

# List Namespaces
path "sys/namespaces*" {
  capabilities = ["read", "list", "sudo"]
}

# List Leases
path "sys/leases" {
  capabilities = ["read", "list", "sudo"]
}

# List Leases
path "sys/leases/*" {
  capabilities = ["read", "list", "sudo"]
}

# Read and List entities and groups
path "identity/*" {
   capabilities = ["read", "list"]
}
