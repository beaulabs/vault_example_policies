#This policy is designed for the root administrator policy. This policy holder will have access to all sub-namespaces with root privileges.
path "sys/namespaces/*" {
   capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}
# Manage auth methods broadly across namespaces
path "auth/*"
{
  capabilities = ["read", "list"]
  }
# List, create, delete, and update auth methods
path "sys/auth"
{
  capabilities = ["read", "list"]
  }
# Manage Secrets via CLI (Root users may be able to create/edit/view secrets)
path "kv/*"
{
  capabilities = ["read", "list"]
  }
# Manage Secrets via CLI
path "kv"
{
  capabilities = ["read", "list"]
  }
# Manage policies via API
path "sys/policies/*"
{
  capabilities = ["read", "list"]
  }
# Manage policies via CLI
path "sys/policy/*"
{
  capabilities = ["read", "list"]
  }
# Enable and manage secrets engines
path "sys/mounts/*"
{
  capabilities = ["read", "list"]
  }
# Create and manage entities and groups
path "identity/*"
{
  capabilities = ["read", "list"]
  }
# Manage Tokens
path "auth/tokens/*"
{
  capabilities = ["read", "list"]
  }
# Read Health Checks
path "sys/health"
{
  capabilities = ["read"]
  }
