# dev1admin.hcl
# List audit backends
path "sys/audit" {
  capabilities = ["read","list"]
}

# Read / List audit backend. Operators are not allowed to remove or write to them.
path "sys/audit/*" {
  capabilities = ["read","list","sudo"]
}

# List Authentication Backends
path "sys/auth" {
  capabilities = ["read","list","sudo"]
}

# CRU operations on Authentication Backends
path "sys/auth/*" {
  capabilities = ["read","list","update","sudo"]
}

# CORS configuration
path "sys/config/cors" {
  capabilities = ["read", "list", "update", "sudo"]
}

# Configure License
path "sys/license" {
  capabilities = ["read", "list", "update"]
}

# Get Storage Key Status
path "sys/key-status" {
  capabilities = ["read"]
}

# Get Cluster Leader
path "sys/leader" {
  capabilities = ["read"]
}

# Manage policies
path "sys/policies*" {
  capabilities = ["read", "list", "update", "delete"]
}

# Manage Mounts
path "sys/mounts*" {
  capabilities = ["read", "list", "update", "delete"]
}

# Manage Namespaces
path "sys/namespaces*" {
  capabilities = ["read", "list", "update", "sudo"]
}

# List Leases
path "sys/leases" {
  capabilities = ["read", "list", "update", "sudo"]
}
#
# Manage Leases
path "sys/leases/*" {
  capabilities = ["read", "list", "update", "sudo"]
}

# Create and manage entities and groups
path "identity/*" {
   capabilities = ["read", "update", "delete", "list"]
}
